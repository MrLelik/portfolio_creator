Build project
```
docker-compose build
```
Run project
```
docker-compose up -d
```
Composer install
```
docker-compose exec php-fpm composer install
```
Run migrations
```
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate
```
Run fixtures
```
docker-compose exec php-fpm php bin/console doctrine:fixtures:load
```
Site link
```
http://localhost:9200/
```