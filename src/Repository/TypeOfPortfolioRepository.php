<?php

namespace App\Repository;

use App\Entity\TypeOfPortfolio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeOfPortfolio|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfPortfolio|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfPortfolio[]    findAll()
 * @method TypeOfPortfolio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfPortfolioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeOfPortfolio::class);
    }

    // /**
    //  * @return TypeOfPortfolio[] Returns an array of TypeOfPortfolio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOfPortfolio
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
