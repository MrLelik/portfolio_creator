<?php

namespace App\Entity;

use App\Repository\PortfolioRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PortfolioRepository::class)
 */
class Portfolio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    private $portfolioNumber;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="50")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="50")
     */
    private $surname;

    /**
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=16)
     *
     * Assert\Luhn(message="Invalid card number.")
     */
    private $bankcardNumber;

    /**
     * @ORM\Column(type="smallint")
     *
     * @Assert\Length(min="4", max="4")
     */
    private $cvv;

    /**
     * @ORM\ManyToOne(targetEntity=TypeOfPortfolio::class, inversedBy="portfolios")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPortfolioNumber(): ?int
    {
        return $this->portfolioNumber;
    }

    public function setPortfolioNumber(int $portfolioNumber): self
    {
        $this->portfolioNumber = $portfolioNumber;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getBankcardNumber(): ?int
    {
        return $this->bankcardNumber;
    }

    public function setBankcardNumber(int $bankcardNumber): self
    {
        $this->bankcardNumber = $bankcardNumber;

        return $this;
    }

    public function getCvv(): ?int
    {
        return $this->cvv;
    }

    public function setCvv(int $cvv): self
    {
        $this->cvv = $cvv;

        return $this;
    }

    public function getType(): ?TypeOfPortfolio
    {
        return $this->type;
    }

    public function setType(?TypeOfPortfolio $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
