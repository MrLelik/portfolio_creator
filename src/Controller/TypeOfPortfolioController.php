<?php

namespace App\Controller;

use App\Entity\TypeOfPortfolio;
use App\Form\TypeOfPortfolioType;
use App\Repository\TypeOfPortfolioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/of/portfolio")
 */
class TypeOfPortfolioController extends AbstractController
{
    /**
     * @Route("/", name="type_of_portfolio_index", methods={"GET"})
     * @param TypeOfPortfolioRepository $typeOfPortfolioRepository
     *
     * @return Response
     */
    public function index(TypeOfPortfolioRepository $typeOfPortfolioRepository): Response
    {
        return $this->render('type_of_portfolio/index.html.twig', [
            'type_of_portfolios' => $typeOfPortfolioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_of_portfolio_new", methods={"GET","POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function new(Request $request): Response
    {
        $typeOfPortfolio = new TypeOfPortfolio();
        $form = $this->createForm(TypeOfPortfolioType::class, $typeOfPortfolio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeOfPortfolio);
            $entityManager->flush();

            return $this->redirectToRoute('type_of_portfolio_index');
        }

        return $this->render('type_of_portfolio/new.html.twig', [
            'type_of_portfolio' => $typeOfPortfolio,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_of_portfolio_show", methods={"GET"})
     * @param TypeOfPortfolio $typeOfPortfolio
     *
     * @return Response
     */
    public function show(TypeOfPortfolio $typeOfPortfolio): Response
    {
        return $this->render('type_of_portfolio/show.html.twig', [
            'type_of_portfolio' => $typeOfPortfolio,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_of_portfolio_edit", methods={"GET","POST"})
     * @param Request         $request
     * @param TypeOfPortfolio $typeOfPortfolio
     *
     * @return Response
     */
    public function edit(Request $request, TypeOfPortfolio $typeOfPortfolio): Response
    {
        $form = $this->createForm(TypeOfPortfolioType::class, $typeOfPortfolio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_of_portfolio_index');
        }

        return $this->render('type_of_portfolio/edit.html.twig', [
            'type_of_portfolio' => $typeOfPortfolio,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_of_portfolio_delete", methods={"DELETE"})
     * @param Request         $request
     * @param TypeOfPortfolio $typeOfPortfolio
     *
     * @return Response
     */
    public function delete(Request $request, TypeOfPortfolio $typeOfPortfolio): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeOfPortfolio->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeOfPortfolio);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_of_portfolio_index');
    }
}
