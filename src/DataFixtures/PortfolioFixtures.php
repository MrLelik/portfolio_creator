<?php

namespace App\DataFixtures;

use App\Entity\Portfolio;
use App\Entity\TypeOfPortfolio;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PortfolioFixtures extends BaseFixture implements DependentFixtureInterface
{

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Portfolio::class, 20, function(Portfolio $portfolio) {
            $portfolio->setPortfolioNumber($this->faker->randomNumber());
            $portfolio->setName($this->faker->firstName);
            $portfolio->setSurname($this->faker->lastName);
            $portfolio->setAddress($this->faker->address);
            $portfolio->setBankcardNumber($this->faker->creditCardNumber);
            $portfolio->setCvv($this->faker->biasedNumberBetween(0, 9999));
            $portfolio->setType($this->getRandomReference(TypeOfPortfolio::class));
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [TypeOfPortfolioFixtures::class];
    }
}
