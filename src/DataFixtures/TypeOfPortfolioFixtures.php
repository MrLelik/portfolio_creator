<?php

namespace App\DataFixtures;

use App\Entity\TypeOfPortfolio;
use Doctrine\Persistence\ObjectManager;

class TypeOfPortfolioFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(TypeOfPortfolio::class, 5, function(TypeOfPortfolio $type) {
            $type->setName($this->faker->jobTitle);
            $type->setDescription($this->faker->realText(20));
        });

        $manager->flush();
    }
}
